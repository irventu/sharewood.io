<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Entity extends Model
{
    public function user() {
        return $this->belongsTo('App\User');
    }

    public function photos()
    {
        return $this->morphMany('App\Photo', 'photoable');
    }

    public function prices() {
        return $this->belongsToMany('App\PriceUnit','entity_price')
                    ->withPivot('price');
        ;
    }
}
