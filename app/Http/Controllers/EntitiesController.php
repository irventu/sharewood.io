<?php

namespace App\Http\Controllers;

use App\Entity;
use Illuminate\Http\Request;

class EntitiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Entity::with('user.locations','photos')->get();
    }

    public function search(Request $request)
    {
        $place = $request->input('place');

        $entities = Entity::whereHas('user.locations', function ($query) use ($request, $place) {
            $query->where('locations.name', $place);
        })->with('user')->get();

        $locations = \App\Location::whereHas('users.entities')->groupBy('name')->select('name')->get();


        return view('entities',
            ['place'=> $place,
             'entities'=> $entities,
             'locations'=>$locations]
        );
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Entity  $entity
     * @return \Illuminate\Http\Response
     */
    public function show(Entity $entity)
    {

        return view('entity', ['entity'=>$entity->load('prices','user.locations')]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Entity  $entity
     * @return \Illuminate\Http\Response
     */
    public function edit(Entity $entity)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Entity  $entity
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Entity $entity)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Entity  $entity
     * @return \Illuminate\Http\Response
     */
    public function destroy(Entity $entity)
    {
        //
    }
}
