<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $locations = \App\Location::groupBy('name')->whereHas('users.entities')->select('name')->get();

        $place_array = [];
        foreach ($locations as $location) {
            $pieces        = explode(',', $location->name);
            $place_array[$pieces[1]][] = $pieces    [0];
        }
        ksort($place_array);
        return view('welcome', ['locations' => $place_array]);
    }
}
