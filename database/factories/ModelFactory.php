<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$faker_it = Faker\Factory::create('it_IT');

$factory->define(App\User::class, function (Faker\Generator $faker) use ($faker_it) {
    static $password;

    return [
        'name'           => $faker_it->name,
        'email'          => $faker_it->unique()->safeEmail,
        'password'       => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
        'type'           => rand(1, 2)

    ];
});

$factory->define(App\Entity::class, function (Faker\Generator $faker) use ($faker_it){

    return [
        'item_description' => $faker_it->paragraph(10),
        'user_id'          => \App\User::all()->random()->id,
    ];

});

$factory->define(App\Location::class, function (Faker\Generator $faker) use ($faker_it) {

    return ['name' => $faker_it->city().', '. $faker_it->state() ];

});


$factory->define(App\Photo::class, function (Faker\Generator $faker)  {
    return ['url' => $faker->imageUrl('400',200,'sports'), 'description' => $faker->paragraph(1) ];
});

