<?php

use Illuminate\Database\Seeder;

class PricesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $units  = [
            [ 'unit' => 'hour'],
            [ 'unit' => 'day'],
            [ 'unit' => 'week'],
            [ 'unit' => 'month']
        ];

        foreach ($units as $unit) {
            \App\PriceUnit::create($unit);
        }

    }
}
