<?php

use Illuminate\Database\Seeder;

class EntitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         factory(App\Entity::class, 200)->create()->each(function($entity){
             for ($i=1; $i<rand(2,5); $i++) {
                 $entity->photos()->save(factory(App\Photo::class)->make());
             }

             for ($i=1; $i<rand(2,5); $i++) {
                 $entity->prices()->attach($i, ['price'=> rand(1,4) * ($i*3) ]);
             }

             $entity->user()->associate(App\User::get()->random());
         });

    }
}