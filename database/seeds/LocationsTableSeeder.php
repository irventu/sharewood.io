<?php

use Illuminate\Database\Seeder;

class LocationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        factory(App\Location::class, 1000)->create()->each(function ($location) {
            $location->users()->attach(App\User::all()->random()->id);
        });

    }
}
