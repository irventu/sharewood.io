@extends('layouts.app')

@section('content')

    <h3>Results for {{  $place }}:</h3>
    @foreach($entities as $entity)
        <div class="row">
            <div class="card">
                <div class="row">
                    <div class="col s4 m4">
                        <img class="avatar" src="{{ $entity->user->photo->url }}" width="30" height="30">
                    </div>
                    <div class="col s8 m8"><h4>Provider: {{$entity->user->name}}</h4>
                    </div>
                    <div class="col s7">
                        <div class="col s12"><strong>Description:</strong> {{ $entity->item_description }}</div>
                    </div>
                    <div class="col s2 dotted-right-border">
                        <img src="{{ $entity->photos->first()->url }}" width="160" height="80">
                    </div>

                    <div class="col m3">
                        @foreach($entity->prices as $price)
                            <div class="row">
                                <div class="col-md-10">
                                    <div class="badge blue col-md-10">
                                        &euro;{{ $price->pivot->price }} per {{ $price->unit }}
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>


                <div class="row padded-bottom">
                    <div class="col-lg-offset-11 col-s12 m12">
                        <a href="/entities/{{ $entity->id }}" class="btn blue">Rent</a>
                    </div>
                </div>


            </div>
        </div>
    @endforeach
    @foreach($locations as $location)
        <a href="/find_equipment?place={{$location->name}}">{{$location->name }}</a> |
    @endforeach

@endsection