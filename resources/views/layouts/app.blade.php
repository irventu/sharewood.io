<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.2/css/materialize.min.css">
    <link rel="stylesheet" href="/css/blueimp-gallery.min.css">

    <div id="blueimp-gallery" class="blueimp-gallery">
        <div class="slides"></div>
        <h3 class="title"></h3>
        <a class="prev">‹</a>
        <a class="next">›</a>
        <a class="close">×</a>
        <a class="play-pause"></a>
        <ol class="indicator"></ol>
    </div>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
<div id="app">
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">

                <a class="navbar-brand" href="{{ url('/') }}">
               <img src="/images/logo-compact-white.png">
                </a>
            </div>





        </div>
    </nav>
    <div class="container">
        @yield('content')
    </div>

</div>

<!-- Scripts -->
<script src="/js/blueimp-gallery.min.js"></script>
<script src="{{ asset('js/app.js') }}"></script>
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.2/js/materialize.min.js"></script>--}}

<script src="/js/datepicker/js/bootstrap-datepicker.min.js"></script>
<link id="bsdp-css" href="/js/datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet">

</body>
</html>
