@extends('layouts.app')

@section('content')

    <div class="card padded-bottom">
        <div class="card-header">
            <h2 class="col-md-offset-5">Location: {{ $entity->user->locations->first->name->name  }}</h2>
        </div>
        <div class="card-block">
            <div class="row padded-bottom">
                <div class="col m4 s12">
                    <div class="col m12">
                        Description: {{ $entity->item_description }}
                    </div>

                </div>

                <div class="col m3 s12 dotted-right-border" >
                    <div id="links">
                        @foreach($entity->photos as $photo)
                            <a href="{{ $photo->url  }}">
                                <img src="{{ $photo->url  }}" width="200" height="100">
                            </a>
                        @endforeach
                    </div>
                </div>
                <div class="col m5">
                    <div class="row">
                        <div class="col m12" data-entity-id="{{ $entity->id }}">
                            <h2>Want to rent this?</h2>
                        </div>
                    </div>
                    <div class="row steps special-row" id="step1">
                        <div class="col m2">
                            <div class="big-number">1</div>
                        </div>
                        <div class="col m5">
                            <h4>Select Start Date</h4>
                        </div>
                        <div class="col m5">
                            <input type="text" class="datepicker datepickerformat trigger-step" id="start-date" data-entity-id="1" />

                        </div>

                    </div>
                    <div class="row steps special-row" id="step2">
                        <div class="col m2">
                            <div class="big-number">2</div>
                        </div>
                        <div class="col m5">
                            <h4>Select End Date</h4>
                        </div>
                        <div class="col m5">
                            <input type="text" class="datepicker datepickerformat trigger-step" id="end-date" data-entity-id="2"/>
                        </div>

                    </div>
                    <div class="row steps special-row" id="step3">

                            <div class="col m2">
                                <div class="big-number">3</div>
                            </div>
                            <div class="col m6">
                                <h4 class>Select Quantity of Items</h4>
                            </div>
                            <div class="col m4">
                                <input type="number" id="number-of-items" size="20" cols="10" class="trigger-step" value="1">
                            </div>
                            <div class="col m12">
                                <button class="btn rental-step blue pull-right" id="calculate-total">Rent</button>
                            </div>
                            <div id="total-days" class="col m12">
                            </div>

                    </div>
                </div>
            </div>

            <div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-carousel">
                <div class="slides"></div>
                <h3 class="title"></h3>
                <a class="prev">‹</a>
                <a class="next">›</a>
                <a class="close">×</a>
                <a class="play-pause"></a>
                <ol class="indicator"></ol>
            </div>
            <div class="row">

                <div class="col-md-12">
                    @foreach($entity->prices as $price)
                        <div class="col-md-3">
                            <div class="badge gray col-md-10">
                                &euro;{{ $price->pivot->price }} per {{ $price->unit }}
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>

    </div>
    <div class="card padded-bottom">
        <div class="row">
            <div class="col m1 s1">
                <img src="{{ $entity->user->photo->url }}" class="avatar" width="20" height="20">
            </div>
            <div class="col m11 s11"><strong>Provider: {{ $entity->user->name }}</strong></div>
        </div>
    </div>

    <script language="javascript">
        function rentalStep(id) {
            console.log(id);
           var step = id+1;
           $('#step'+ step).show();
        }

        function calculateTotal(startDate, endDate) {
            startDateObj = new Date(startDate);
            endDateObj = new Date(endDate);
            var numberOfDays = (endDateObj - startDateObj) / (86400 * 1000);
            $('#total-days').html('Total number of days: ' + numberOfDays);
        }

        $(function () {

            $('.datepicker').datepicker({});

            $('.trigger-step').on('change', function (event) {
                if (event.target.value.length > 0) {
                    rentalStep($(event.target).data('entity-id'));
                }
            });

            $('#start').click(function (event) {
                rentalStep($(event.target).data('entity-id'));
            });

            $('.steps').not('#step1').hide();

            $('#calculate-total').click(function (event) {
                calculateTotal($('#start-date').val(), $('#end-date').val());
            });
        });
    </script>

@endsection