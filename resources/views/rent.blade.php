@extends('layouts.app')

@section('content')
    <div class="card padded-bottom">

        <div class="row padded-bottom">
            <div class="col-md-6">{{ $entity->item_description }}</div>
            <div class="col-md-6">
                <div id="links">
                    @foreach($entity->photos as $photo)
                        <a href="{{ $photo->url  }}">
                            <img src="{{ $photo->url  }}" width="200" height="100">
                        </a>
                    @endforeach
                </div>
            </div>
        </div>

        <div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-carousel">
            <div class="slides"></div>
            <h3 class="title"></h3>
            <a class="prev">‹</a>
            <a class="next">›</a>
            <a class="close">×</a>
            <a class="play-pause"></a>
            <ol class="indicator"></ol>
        </div>
        <div class="row">

            <div class="col-md-12">
                @foreach($entity->prices as $price)
                    <div class="col-md-3">
                        <div class="badge gray col-md-10">
                            &euro; {{ $price->pivot->price }} per {{ $price->unit }}
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        {{--<a href="/entities/{{ $entity->id }}">Rent</a></p>--}}
    </div>
        <div class="card padded-bottom">
            <div class="row">

            <div class="col m1 s1">
                <img src="{{ $entity->user->photo->url }}" width="20" height="20">
            </div>
            <div class="col m11 s11"><strong>Provider: {{ $entity->user->name }}</strong></div>

        </div>
    </div>
@endsection