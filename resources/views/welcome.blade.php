@extends('layouts.app')

@section('content')

    <div class="flex-center position-ref full-height">
        <div class="container">
            <div class="card">
                <div class="row">
                    <div class="col m10 s12">
                        <form class="" action="find_equipment" method="GET">
                            {{ csrf_field() }}
                            <p class="input-field text-center">
                                <input id="location" name="place" type="text" class="validate"
                                       style="width:400px" placeholder="Where do you need sports equipment?">
                                <label for="first_name"></label>
                                <button id="location_search" disabled type="submit"
                                        class="btn waves-effect waves-light blue"
                                        type="button" name="action">Search
                                </button>
                            </p>
                        </form>
                    </div>
                </div>
            </div>
            <div class="row">
                @foreach ($locations as $key => $val)
                    <div class="col m12 s12"><h4>{{$key }}</h4></div>
                    @foreach ($val as $value)
                        <div class="col m3 s3 pull-left"><a
                                    href="/find_equipment?place={{$value}}, {{ $key }}"> {{$value}} </a></div>
                    @endforeach
                @endforeach
            </div>
        </div>
    </div>
    <script>
        $(function () {
            $('#location').on('keyup', function (event) {
                if (event.target.value.length > 2) {
                    $('#location_search').removeAttr('disabled');
                } else {
                    $('#location_search').attr('disabled', true);
                }
            });
        });
    </script>

@endsection
