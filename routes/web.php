<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');
Route::resource('entities', EntitiesController::class);
Route::get('find_equipment', 'EntitiesController@search');
Route::get('rent', 'EntitiesController@search');
Auth::routes();
